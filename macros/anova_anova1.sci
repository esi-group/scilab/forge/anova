// Copyright (C) 2013 - Holger Nahrstaedt
// Copyright (C) 1995-2011 Kurt Hornik
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution. The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [pval, f, df_b, df_w] = anova_anova1 (y, g,alpha)
// Perform a one-way analysis of variance (ANOVA)
// Calling Sequence
// [pval, f, df_b, df_w] = anova_anova1 (y)
// [pval, f, df_b, df_w] = anova_anova1 (y,alpha)
// [pval, f, df_b, df_w] = anova_anova1 (y,g)
// [pval, f, df_b, df_w] = anova_anova1 (y,g,alpha)
// Parameters
// 
// Description
// Perform a one-way analysis of variance (ANOVA).  The goal is to test
// whether the population means of data taken from k different
// groups are all equal.
//
// Data may be given in a single vector y with groups specified by
// a corresponding vector of group labels g (e.g., numbers from 1
// to k).  This is the general form which does not impose any
// restriction on the number of data in each group or the group labels.
//
// If y is a matrix and g is omitted, each column of y
// is treated as a group.  This form is only appropriate for balanced
// ANOVA in which the numbers of samples from each group are all equal.
//
// Under the null of constant means, the statistic f follows an F
// distribution with df_b and df_w degrees of freedom.
//
// The p-value (1 minus the CDF of this distribution at f) is
// returned in pval.
//
// If no output argument is given, the standard one-way ANOVA table is
// printed.
// Examples
// y=[17,25,22,26;19,27,21,24;20,18,19,30;24,22,26,28];
// anova_anova1(y,0.01);
// 
// One-way ANOVA Table:
// 
// Source of Variation   Sum of Squares    df  Empirical Var
// *********************************************************
// Between Groups              104.0000     3        34.6667
// Within Groups               118.0000    12         9.8333
// ---------------------------------------------------------
// Total                       222.0000    15
// 
// Test Statistic f              3.5254
// p-value                       0.0487
// Fcrit (alpha=0.0100)          5.9525
// 
// y_list=list(y(:,1),y(:,2),y(:,3),y(:,4));
// anova_anova1(y_list,0.01);
// 
// y_vec=y(:);
// groups=[1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4];
// anova_anova1(y_vec,groups,0.01);
//
// Authors
// Copyright (C) 1995-2011 Kurt Hornik
// H. Nahrstaedt - 2012



  [lhs,rhs]=argn();
  apifun_checkrhs ( "anova_anova1" , rhs , 1:3 );
  apifun_checklhs ( "anova_anova1" , lhs , 0:4 )



  

  if (rhs < 3) & type(y)==[15] then
    if (length (y)==1)
      error ("anova_anova1: for anova (Y), Y must not be a vector");
    end
    if rhs==2 then
      alpha=g;
    else
      alpha=[];
    end;
    k=length(y);
    group_count=zeros(1,k);
    group_mean=zeros(1,k);
    n=0;
    y_tmp=y;
    y=[];
    for i=1:k
      group_count(i)=length(thrownan(y_tmp(i)));
      group_mean(i) = mean (thrownan(y_tmp(i)));
      n=n+group_count(i);
      tmp=y_tmp(i);
      y=[y;tmp(:)];
    end
  elseif (~isvector (y)) & rhs<3 & or(type(y)==[1 5 8]) then
    if (isvector (y))
      error ("anova_anova1: for anova (Y), Y must not be a vector");
    end
    if rhs==2 then
      alpha=g;
    else
      alpha=[];
    end;
    [group_count, k] = size (y);
    n = group_count * k;
    group_mean = nanmean (y,'r');
  else
    if rhs<3 then
      alpha=[];
    end;
    if (~ isvector (y))
      error ("anova_anova1: for anova (Y, G), Y must be a vector");
    end
    n = length (y);
    if (~ isvector (g) | (length (g) ~= n))
      error ("anova_anova1: G must be a vector of the same length as Y");
    end
    // remove parted in which g has nan
    y(find(isnan(g)))=[];
    g(find(isnan(g)))=[];
    n = length (y);
    s = mtlb_sort (g);
    i = find (s (2 : n) > s(1 : (n-1)));
    k = length (i) + 1;
    if (k == 1)
      error ("anova_anova1: there should be at least 2 groups");
    else
      group_label = s ([1, (matrix (i, 1, k-1) + 1)]);
    end
    n=0
    group_count=zeros(1,k);
    group_mean=zeros(1,k);
    y_tmp=y;
    y=[];
    for i = 1 : k;
      v = y_tmp (find (g == group_label (i)));
      group_count (i) = length (thrownan(v));
      group_mean (i) = nanmean (thrownan(v));
      n=n+group_count(i);
      y=[y;v(:)];
    end

  end



  total_mean = nanmean (y);
  SSB = nansum (group_count .* (group_mean - total_mean) .^ 2);
  SST = nansum( (thrownan(y) - total_mean).^2);
  SSW = SST - SSB;
  df_b = k - 1;
  df_w = n - k;
  v_b = SSB / df_b;
  v_w = SSW / df_w;
  f = v_b / v_w;
  pval = 1 - distfun_fcdf (f, df_b, df_w);

  //if (length(df_b)==1 & length(df_w)==1)
      //pval = 1- (1 - nan_betainc (1 ./ (1 + df_b .* f ./ df_w), df_w / 2, df_b / 2));


      
 if ~isempty(alpha)
  fcrit=cdff("F",df_b,df_w,1-alpha,alpha)
 end;

  //if (nargout == 0)
    // This eventually needs to be done more cleanly ...
    printf ("\n");
    printf ("One-way ANOVA Table:\n");
    printf ("\n");
    printf ("Source of Variation   Sum of Squares    df  Empirical Var\n");
    printf ("*********************************************************\n");
    printf ("Between Groups       %15.4f  %4d  %13.4f\n", SSB, df_b, v_b);
    printf ("Within Groups        %15.4f  %4d  %13.4f\n", SSW, df_w, v_w);
    printf ("---------------------------------------------------------\n");
    printf ("Total                %15.4f  %4d\n", SST, n - 1);
    printf ("\n");
    printf ("Test Statistic f     %15.4f\n", f);
    printf ("p-value              %15.4f\n", pval);
    if ~isempty(alpha) then
    printf ("Fcrit (alpha=%1.4f) %15.4f\n",alpha,fcrit);
    end;
    printf ("\n");
  //end

endfunction
